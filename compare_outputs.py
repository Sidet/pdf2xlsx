# Compares output data with etalons
import os

import openpyxl


def get_comparison_files(path):
    files = [path + '\\' + file for file in os.listdir(path)]
    comp_files = []
    for file in files:
        if file.split('\\')[-1].startswith("Comp_") and\
                file.endswith('.xlsx'):
            comp_files.append(file)
    return comp_files


def compare_xlsx(path):
    basepath = '\\'.join(path.split('\\')[:-1])
    filename1 = path.split('\\')[-1]
    file1 = path
    filename2 = filename1[5:]
    file2 = basepath + '\\' + filename2

    if os.path.isfile(file1):
        wb1 = openpyxl.load_workbook(file1)
    else:
        return (file1, None)

    if os.path.isfile(file2):
        wb2 = openpyxl.load_workbook(file2)
    else:
        return (file2, None)

    differences = []
    for r1, r2 in zip(wb1.active, wb2.active):
        for c1, c2 in zip(r1, r2):
            if c1.value != c2.value:
                differences.append([c1, c2])

    report_file = basepath + '\\' + '.'.join(file1.split('.')[:-1]) + '.txt'
    if differences:
        lines = []
        for d in differences:
            msg = f'Cell {d[0].coordinate} is "{d[1].value}" in new file.'
            msg += f' Compare file provided "{d[0].value}"'
            msg += '\r\n'
            lines.append(msg)
        with open(report_file, 'w', encoding='UTF8') as f:
            f.writelines(lines)
        return (file2, len(differences))
    else:
        if os.path.isfile(report_file):
            os.remove(report_file)
        return (file2, True)


def compare_files(files):
    status = [0, 0]
    output = []
    for path in files:
        res = compare_xlsx(path)
        if res[1] is True:
            status[0] += 1
        elif isinstance(res[1], int):
            status[1] += 1
        output.append(res)
    return status, output


def print_report(output, reporting):
    for res in output:
        if res[1] is True and reporting in ['verbose']:
            print(f'No differences found in {res[0]}')
        elif res[1] is None and reporting in ['verbose', 'errors_only']:
            print(f'File {res[0]} not found.')
        elif reporting in ['verbose', 'errors_only']:
            print(f'Found {res[1]} differences in {res[0]}')


def print_completion(test_passed, test_failed):
    count = test_passed + test_failed
    print(f'{count} comparisons complete.')
    if test_failed:
        print(f'Differences found in {test_failed} files.')
    elif test_passed:
        print(f'All file pairs are identical.')


def main(path, reporting='brief'):
    files = get_comparison_files(path)
    status, output = compare_files(files)
    print_report(output, reporting)
    print_completion(*status)


if __name__ == '__main__':
    main(path=os.getcwd() + '\\' + 'Data', reporting='verbose')
