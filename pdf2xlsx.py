import string
import os
import sys
import io
import traceback
import xml.etree.ElementTree

import openpyxl
import pdfminer.high_level


LTU_UPPERCASE = 'AĄBCČDEĘĖFGHIĮYJKLMNOPRSŠTUŲŪVZŽ'
LTU_LOWERCASE = 'aąbcčdeęėfghiįyjklmnoprsštuųūvzž'
LTU_LETTERS = LTU_UPPERCASE + LTU_LOWERCASE
FPATH = os.path.dirname(__file__) + "\\"
ROW_SPACING = 3


class Element():

    def __init__(self, bbox, text):
        x1, y1, x2, y2 = bbox
        self.left = x1
        self.right = x2
        self.bottom = y1
        self.top = y2
        self.text = text
        self.row = 0
        self.col = 0
        self.children = []
        self.depth = 0

    def __repr__(self):
        return str(self.text)

    def __eq__(self, other):
        return self.text == other.text

    @property
    def bbox(self):
        return [self.left, self.bottom, self.right, self.top]

    @bbox.setter
    def bbox(self, ls):
        self.left = ls[0]
        self.bottom = ls[1]
        self.right = ls[2]
        self.top = ls[3]


class Row(list):

    def __init__(self, bottom, top, content=None):

        if content is None:
            content = []
        elif not isinstance(content, list):
            content = [content]

        super().__init__(content)
        self.top = top
        self.bottom = bottom
        self.n = 0


def pdf_to_text(fname, reporting=False):
    """
    Takes full file path without extension, returns list of extracted text
    elements. List elements are in [coordinates_list, content] form.
    """
    return xml_to_text(pdf_to_xml(fname, reporting))


def pdf_to_xml(fname, reporting=False):
    """
    Creates an xml file from which
    """
    xml_data = io.BytesIO()
    if reporting:
        print(f'Apdorojamas "{fname}.pdf"')

    with open(fname + '.pdf', 'rb') as infp:
        pdfminer.high_level.extract_text_to_fp(
            infp,
            xml_data,
            output_type='xml',
            boxes_flow=True,
            all_texts=True)

    xml_data.seek(0)
    return xml_data


def xml_to_text(xml_data):
    tree = xml.etree.ElementTree.parse(xml_data)
    # pages = tree.getroot().getchildren()
    text_pages = []
    for n, p in enumerate(tree.getroot(), 1):
        page = []
        letters = []
        for e in p:
            if (e.tag in ['rect', 'line'] or
                    e.text == '|' or
                    e.text is None):
                if not letters == []:
                    add_text_line(letters, page)
                    letters = []
                continue
            elif letters == []:
                pass
            elif not same_text_line(letters, e):
                add_text_line(letters, page)
                letters = []
            letters.append([get_bbox_corners(e.get('bbox')), e.text])
        if not letters == []:
            add_text_line(letters, page)
            letters = []
        if page:
            page.sort(key=lambda x: x.top, reverse=True)
            text_pages.append(page)
    # Combines separate warped text into same object
    for page in text_pages:
        for n, e in enumerate(page):
            if isinstance(e.text, str) and (n - 1) < len(page):
                to_be_removed = []
                for m, t in enumerate(page[n + 1:], n + 1):
                    if isinstance(t.text, str):
                        if is_warped_text(e, t):
                            to_be_removed.append(m)
                            combine_warped_text(e, t)
                for i in reversed(to_be_removed):
                    page.pop(i)
    return text_pages


def add_text_line(letters, page, letter_list=None):
    """
    Assembles strings from separate characters and adds them to text line
    list while removing unwanted information ('----' fargments and extra
    spaces).
    """
    if letter_list:
        bbox = get_bbox_corners(letter_list.get('bbox'))
    else:
        bbox = build_bbox(letters)
    text = join_letters_in_order(letters)
    if '--' in text or '__' in text:
        text = ''
    text = text.strip()
    if text:
        page.append(Element(bbox, string_to_number(text)))


def get_bbox_corners(string):
    return [float(i) for i in string.split(',')]


def build_bbox(objects):
    bbox = [20000, 20000, 0, 0]
    for o in objects:
        x1, y1, x2, y2 = o[0]
        if bbox[0] > x1:
            bbox[0] = x1
        if bbox[1] > y1:
            bbox[1] = y1
        if bbox[2] < x2:
            bbox[2] = x2
        if bbox[3] < y2:
            bbox[3] = y2
    return bbox


def join_letters_in_order(letters):
    """
    Sorts list based on first element (x coordinate) and joins only characters
    into string. Coordinates are discarder.
    """
    return ''.join([i[1] for i in sorted(letters)])


def is_warped_text(o1, o2):
    """
    Determines if second objects text is continuation of first objects text.
    """
    lowercase = o2.text[0] in LTU_LOWERCASE
    envelops12 = o1.left <= o2.left and o1.right >= o2.right
    envelops21 = o2.left <= o1.left and o2.right >= o1.right
    close_lines = (o1.bottom - o2.top) < ROW_SPACING
    return close_lines and (envelops12 or envelops21) and lowercase


def combine_warped_text(o1, o2):
    if o1.text[-1] == '-':
        o1.text = o1.text[:-1] + o2.text
    else:
        o1.text += ' ' + o2.text
    if o2.left < o1.left:
        o1.left = o2.left
    if o2.right > o1.right:
        o1.right = o2.right
    o1.bottom = o2.bottom


def determine_text_rows(text_pages):
    """
    Creates a lists of elements separated into pages. Within pages
    elements are assigned to same group if they belong to same line.
    grid_pages = [[Row(), Row(), ...], [[Row(), Row(), ...], ...]
    """
    pages_with_rows = []
    for page in text_pages:
        rows = []
        for elem in page:
            determine_element_row(rows, elem)
        pages_with_rows.append(rows)
    return pages_with_rows


def determine_element_row(rows, element):
    """
    Adds element to matching line list or creates new line list with new
    bounds if no matching line is found.
    """
    # PDF coordinates origin is bottom-left
    x1, y1, x2, y2 = element.bbox
    found = False
    for row in rows:
        if matches_line(element.bbox, [row.bottom, row.top]):
            if y1 < row.bottom:
                row.bottom = y1
            row.append(element)
            found = True
            break
    if not found:
        rows.append(Row(y1, y2, [element]))


def same_text_line(letters, obj):
    ox1, oy1, ox2, oy2 = get_bbox_corners(obj.get('bbox'))
    no_gap = abs(ox1 - letters[-1][0][2]) < 2
    return matches_line(letters[-1][0], [oy1, oy2]) and no_gap


def matches_line(obj_bbox, line):
    y1, y2 = obj_bbox[1], obj_bbox[3]
    bottom, top = line
    y1_inside = y1 >= bottom and y1 <= top
    y2_inside = y2 >= bottom and y2 <= top
    envelops = y1 < bottom and y2 > top
    return y1_inside or y2_inside or envelops


def matches_column(obj_bbox, col):
    x1, x2 = obj_bbox[0], obj_bbox[2]
    left, right = col[0], col[1]
    x1_inside = (x1 >= left and x1 <= right)
    x2_inside = (x2 >= left and x2 <= right)
    envelops = (x1 < left and x2 > right)
    return x1_inside or x2_inside or envelops


def find_header_row(text_rows):
    hr = Row(0, 0)
    i_hr = -1

    # Row with most elements is assumed to be header
    for n, row in enumerate(text_rows):
        if has_strings(row) and len(row) > len(hr):
            hr = row
            i_hr = n
    hr = text_rows.pop(i_hr)

    # Check if header part bellow was not included
    if (i_hr < len(text_rows) and only_strings(text_rows[i_hr]) and
            hr.bottom - text_rows[i_hr].top < 10 and
            len(text_rows[i_hr]) > 1):
        ar = text_rows.pop(i_hr)
        hr = Row(hr.bottom + ar.bottom, hr.top, hr + ar)
        hr.sort(key=lambda x: (x.top + x.bottom) / 2, reverse=True)

    # Analyze header structure
    depth = 0
    for n, e in enumerate(reversed(hr)):
        if n:
            for element in reversed(hr[len(hr) - n:]):
                if matches_column(element.bbox, [e.left, e.right]):
                    if e.depth <= element.depth:
                        e.depth = element.depth + 1
                        if depth < e.depth:
                            depth = e.depth

    # Split header into rows based on parent count/depth
    temp_rows = [[] for _ in range(depth + 1)]
    for e in hr:
        temp_rows[e.depth].append(e)
    while len(temp_rows) > 1:
        tr = temp_rows.pop()
        text_rows.append(Row(tr[0].bottom, tr[0].top, tr))
    hr = temp_rows.pop()
    hr.sort(key=lambda x: x.left)
    hr = Row(hr[0].bottom, hr[0].top, hr)
    text_rows.append(hr)
    return hr


def has_strings(row):
    for e in row:
        if isinstance(e.text, str):
            return True
    return False


def only_strings(row):
    for e in row:
        if not isinstance(e.text, str):
            return False
    return True


def assign_cell_coords(pages_without_headers, header_row):
    for page in pages_without_headers:
        page.sort(key=lambda x: (x.top + x.bottom) / 2, reverse=True)
        column_bounds = get_column_bounds(header_row)
        for n, row in enumerate(page, 1):
            row.n = n
            cols = [[] for _ in range(len(header_row) + 1)]
            for e in row:
                e.row = n
                x = (e.left + e.right) / 2
                m = get_column_number(x, column_bounds)
                cols[m].append(e)
                e.col = m
            fix_overlap_cols(cols)


def get_column_bounds(row):
    column_bounds = []
    bound = 0
    for n, e in enumerate(row):
        if n + 1 == len(row):
            bound = 999999
        else:
            bound = (row[n].right + row[n + 1].left) / 2
        column_bounds.append(bound)
    return column_bounds


def get_column_number(x, column_bounds):
    for n, bound in enumerate(column_bounds, 1):
        if x < bound:
            return n


def fix_overlap_cols(row):
    """
    Takes a list of cells, where cells are lists of Element() objects.
    Cell should have only one element. Run this method to fix any overlaps.
    """
    for n, cell in enumerate(row):
        if len(cell) == 2:  # If cell has two elements.
            shift_list = []
            # Element that starts more to the left is put to shift_list
            if cell[0].left < cell[1].left:
                shift_list.append(cell[0])
            else:
                shift_list.append(cell[1])
            checked = 0
            while checked + 1 < n:
                checked += 1
                u = n - checked
                if len(row[u]) == 0:  # If empty column
                    for i, e in enumerate(reversed(shift_list)):
                        e.col -= 1
                        row[u + i].append(e)
                        row[u + i + 1].remove(e)
                    break
                shift_list.append(row[u][0])
            else:
                shift_list = []
                if cell[0].left < cell[1].left:
                    shift_list.append(cell[1])
                else:
                    shift_list.append(cell[0])
                checked = n
                while checked + 1 < len(row):
                    checked += 1
                    if len(row[checked]) == 0:
                        for i, e in enumerate(reversed(shift_list)):
                            e.col += 1
                            row[checked - i].append(e)
                            row[checked - i - 1].remove(e)
                        break
                    shift_list.append(row[checked][0])
                else:
                    pass
                    # raise NotImplementedError  # No space to expand overlap.
        elif len(cell) >= 3:
            for e in cell:
                if e.text == '/':
                    break
            else:
                # raise NotImplementedError
                # Too many overlaped columns to expand.
                pass


def remove_page_numbers(page_text):
    """
    Removes page number markings. Isoletes row based on '/' symbol and assumes
    everything on the same row needs to be removed.
    """
    found = False
    for elem in page_text:
        if (isinstance(elem.text, str) and
                len(elem.text) == 1 and
                '/' in elem.text):
            row = elem.row
            page_text.remove(elem)
            found = True
            break
    if found:
        to_be_removed = []
        for n, elem in enumerate(page_text):
            if elem.row == row:
                to_be_removed.append(n)
        for n in reversed(to_be_removed):
            page_text.pop(n)


"""
grid_pages[pagenumber][lines (0) or columns (1)]
lines = [[y1, y2, [grouped elements], row or col number], ...]
element = [bbox, text, row number, col number]
"""


def save_excel(data, filename="lent.xlsx"):
    if '\\' in filename:
        path = filename
    else:
        path = FPATH + filename
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = 'PDF data'
    max_row = 0
    row_adjust = 0
    for page in data:
        for elem in page:
            ws.cell(
                row=elem.row + row_adjust, column=elem.col, value=elem.text)
            if elem.row + row_adjust > max_row:
                max_row = elem.row + row_adjust
        row_adjust = max_row
    wb.save(path)


def string_to_number(value):
    """
    Converts number string into int or float. Not number strings are returned
    unchanged.
    """
    if is_number(value):
        value = value.replace(' ', '')
        value = value.replace('.', '')
        if ',' in value:
            value = float(value.replace(',', '.'))
        else:
            value = int(value)
    return value


def is_number(value):
    text = False
    for l in LTU_LETTERS:
        if l in value:
            text = True
            break
    percent = '%' in value
    pos_num = value[0] in string.digits and '-' not in value
    neg_num = value[0] == '-' and value[1] in string.digits
    num = pos_num or neg_num
    interval = '>' in value or ':' in value
    return num and not interval and not percent and not text


def match_header_rows(text_pages):
    page1, page2 = text_pages[0], text_pages[1]
    i, j = 0, 0
    first_not_matching = None
    while len(page1) > i:
        if page1[i].text != page2[j].text:
            if j == 0:
                i += 1
            else:
                first_not_matching = j
                break
        else:
            i += 1
            j += 1

    first_page_different_start = page1[:i - j]
    for _ in range(0, i - j):
        page1.pop(0)

    document_top = page1[:first_not_matching]

    # remove headers from pages
    for n in range(0, len(text_pages)):
        if document_top == text_pages[n][:first_not_matching]:
            text_pages[n] = text_pages[n][first_not_matching:]
        else:
            raise NotImplementedError
    document_top = first_page_different_start + document_top
    return document_top


def remove_duplicate_rows(text_pages, pages_without_headers, header_row=None):
    print(sorted(header_row, key=lambda e: e.left))
    first_page = pages_without_headers[0]
    header_has_numbers = False
    if header_row:
        header_has_numbers = not only_strings(header_row)
    for page, tpage in zip(pages_without_headers[1:], text_pages[1:]):
        duplicates = []
        for test_row in first_page:
            if (header_has_numbers and
                    compare_rows_with_numbers(header_row, test_row)):
                for n, row in enumerate(page):
                    if compare_rows_with_numbers(test_row, row):
                        duplicates.append(n)
                        break
            else:
                for n, row in enumerate(page):
                    if compare_rows(test_row, row):
                        duplicates.append(n)
                        break
        removed_data = []
        for n in reversed(duplicates):
            row = page.pop(n)
            for e in row:
                removed_data.append(e)
        for e in removed_data:
            tpage.remove(e)


def compare_rows(r1, r2):
    """
    Compares text content of Row objects. Ignores bounding boxes.
    If any row has any non strings returns False.
    """
    for a, b in zip(r1, r2):
        if not isinstance(a.text, str) or not isinstance(b.text, str):
            return False
        if a.text != b.text:
            return False
    return True


def compare_rows_with_numbers(r1, r2):
    """
    Compares text content of Row objects. Ignores bounding boxes.
    """
    for a, b in zip(r1, r2):
        if a.text != b.text:
            return False
    else:
        return True


def remove_extension(fn):
    return fn[:len(fn) - 1 - fn[::-1].find('.')]


def filter_pdfs(files):
    if files is None:
        files = sys.argv[1:]
    pdf_files = []
    for path in files:
        if os.path.isfile(path):
            if path.endswith('.pdf'):
                pdf_files.append(path)
        elif os.path.isdir(path):
            for sub_path in os.listdir(path):
                if os.path.isfile(path + '\\' + sub_path) and\
                        sub_path.endswith('.pdf'):
                    pdf_files.append(path + '\\' + sub_path)
    return pdf_files


def main(files=None, reporting=True):
    pdf_files = filter_pdfs(files)

    for full_name in pdf_files:
        fname = remove_extension(full_name)

        text_pages = pdf_to_text(fname, reporting=reporting)

        if len(text_pages) > 1:
            document_header = match_header_rows(text_pages)

        pages_with_rows = determine_text_rows(text_pages)

        if len(text_pages) > 1:
            top_rows = determine_text_rows([document_header])[0]
            table_header = find_header_row(top_rows)
        else:
            table_header = find_header_row(pages_with_rows[0])

        if len(text_pages) > 1:
            text_pages[0] = document_header + text_pages[0]
            pages_with_rows[0] = top_rows + pages_with_rows[0]

        assign_cell_coords(pages_with_rows, table_header)

        for page in text_pages:
            remove_page_numbers(page)

        save_excel(text_pages, fname + '.xlsx')

        if reporting:
            print(f'Rezultatas faile "{fname}.xlsx".')


if __name__ == "__main__":
    error = False
    try:
        main()
    except: # noqa
        error = True
        with open('klaida.log', 'a', encoding='UTF8') as error_log:
            traceback.print_exc(file=error_log)
        print('APDOROJIMO METU ĮVYKO NEŽINOMA KLAIDA! APDOROJIMAS NUTRAUKTAS!')
        input('Paspauskite Enter uždaryti ')
    if not error:
        print('Apdorojimas sėkmingai baigtas.')
        input('Paspauskite Enter uždaryti langui...')
