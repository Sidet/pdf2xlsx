import re

if __name__ == "__main__":
    pattern = re.compile(b'def .*?[(]')
    with open('pdf2xlsx.py', 'rb') as f:
        lines = [line for line in f.readlines()]
    functions = [pattern.search(line) for line in lines
                 if pattern.match(line) is not None]
    functions = [func.group()[4:-1] for func in functions]
    for func in functions:
        p = re.compile(func)
        matches = [p.search(line) for line in lines
                   if p.search(line) is not None]
        if len(matches) == 1:
            print(f'{func.decode()} had {len(matches)} in file')
