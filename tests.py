# test suit for pdf2xlsx
import unittest
from unittest import TestCase

from parameterized import parameterized

import pdf2xlsx


class TestMost(TestCase):
    def test_remove_extension(self):
        result = pdf2xlsx.remove_extension('C:\\Data\\File\\Cookies.mls')
        self.assertEqual('C:\\Data\\File\\Cookies', result)

    @parameterized.expand([
        ([[0, 0, [[0, '5'], [4, 'five']]],
          [0, 0, [[[12, 45, 75, 65.4], '5'], [7, 'five']]]], True),
        ([[1, 1, [[4, 4]]], [1, 1, [[4, '4']]]], False),
    ])
    def test_compare_rows(self, r, expected):
        result = pdf2xlsx.compare_rows(r[0], r[1])
        self.assertEqual(expected, result)


def run_tests():
    unittest.main()


if __name__ == "__main__":
    run_tests()
