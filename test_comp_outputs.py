import unittest

from parameterized import parameterized

import compare_outputs


class TestCompOutput(unittest.TestCase):
    @parameterized.expand([
        (['lentele.pdf', 'raktas.txt', 'comp_.xlsx', 'Comp_lentele.xlsx'],
         ['Comp_lentele.xlsx']),
        ([], []),
        (['Comp_lentele.pdf', 'raktas.txt', 'Comp_.xlsx', 'Comp_lentele.xlsx'],
         ['Comp_.xlsx', 'Comp_lentele.xlsx']),
    ])
    def test_get_comparison_files(self, inpt, expected):
        result = compare_outputs.get_comparison_files(files=inpt)
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
