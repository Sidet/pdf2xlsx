# script for testing pdf2xlsx build
import os

import pdf2xlsx
import compare_outputs

if __name__ == "__main__":
    pdf2xlsx.main(files=[os.getcwd() + '\\Data'], reporting=None)
    compare_outputs.main(os.getcwd() + '\\Data')
